<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[OrderController::class, 'create'])->name("makeOrder");

Route::post('/pay', [PaymentController::class, 'initialize'])->name('pay');
Route::get('/pay', function () { return view('welcome'); });
Route::get('/pay/success', function () { return view('success'); })->name('success');
Route::get('/pay/unsuccessful', function () { return view('unsuccessful'); })->name('unsuccessful');
Route::any('/rave/callback', [PaymentController::class, 'callback'])->name('callback');


Route::get('/dashboard', [OrderController::class, 'index'])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
