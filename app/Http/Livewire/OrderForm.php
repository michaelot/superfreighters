<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Rave;

class OrderForm extends Component
{
    public $name;
    public $email;
    public $origin="UK";
    public $mode="Air";
    public $weight =1;
    public $total = 0;


    public $originAmount =800;
    public $modeAmount=50000;
    public $weightAmount =0;
    public $weightModeAmount =10000;

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
    ];

    public function mount()
    {
        $this->calculateTotal();
    }

    public function updatingMode($value)
    {
        if($value == "Air"){
            $this->modeAmount=50000;
            $this->weightModeAmount =10000;
        }elseif($value == "Sea"){
            $this->modeAmount=15000;
            $this->weightModeAmount =2000;
        }
        $this->calculateWeight($this->weight);
        $this->calculateTotal();
    }

    public function updatingOrigin($value)
    {
        if($value == "UK"){
            $this->originAmount = 800;
        }elseif($value == "USA"){
            $this->originAmount = 1500;
        }
        $this->calculateTotal();
    }

    public function updatingWeight($value)
    {
        $this->calculateWeight($value);
        $this->calculateTotal();
    }

    public function calculateWeight($value){
        $this->weightAmount = (int) $value * $this->weightModeAmount;
    }

    public function calculateTotal(){
        $subtotal = $this->originAmount + $this->modeAmount + $this->weightAmount;
        $tax = $subtotal * 0.1;
        $this->total = $subtotal + $tax;
    }

    public function render()
    {
        return view('livewire.order-form');
    }
}
