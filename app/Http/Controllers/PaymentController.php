<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use KingFlamez\Rave\Facades\Rave;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderSuccess;

class PaymentController extends Controller
{
    public function initialize(Request $request)
    {
        $request->session()->put('name', $request->name);
        $request->session()->put('email', $request->email);
        $request->session()->put('origin', $request->origin);
        $request->session()->put('mode', $request->mode);
        $request->session()->put('weight', $request->weight);

        Rave::initialize(route('callback'));
    }

    public function callback(Request $request)
    {
        $response = json_decode($request['resp']);
        // dd();
        $data = Rave::verifyTransaction($response->tx->txRef);
        $chargeResponsecode = $data->data->chargecode;
        $chargeAmount = $data->data->amount;
        $chargeCurrency = $data->data->currency;

        $amount = $data->data->amount;
        $currency = "NGN";

        if (($chargeResponsecode == "00" || $chargeResponsecode == "0") && ($chargeAmount == $amount)  && ($chargeCurrency == $currency)) {
            $order = new Order();
            $order->name = $request->session()->get('name');
            $order->email = $request->session()->get('email');
            $order->mode = $request->session()->get('mode');
            $order->origin = $request->session()->get('origin');
            $order->weight = $request->session()->get('weight');
            $order->amount = $chargeAmount;
            $order->save();
            $request->session()->flush();

            Mail::to($order->email)->send(new OrderSuccess($order));
            return redirect()->route('success');

      } else {
            return redirect()->route('unsuccessful');
      }
    }
}
