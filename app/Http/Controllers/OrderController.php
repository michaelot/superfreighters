<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Mail\OrderSuccess;



use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){

        $orders = Order::orderBy('id', 'desc')->get();
        return view('dashboard',['orders'=>$orders]);
    }

    public function create(){
        return view('welcome');
    }
}
