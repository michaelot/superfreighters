<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Orders') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white overflow-x-visible border-b border-gray-200">
                    <table class="min-w-max  w-full table-auto">
                        <thead>
                            <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                                <th class="py-3 px-6 text-left">Customer Name</th>
                                <th class="py-3 px-6 text-left">Customer Email</th>
                                <th class="py-3 px-6 text-center">Country of Origin</th>
                                <th class="py-3 px-6 text-center">Mode of Transport</th>
                                <th class="py-3 px-6 text-center">Weight</th>
                                <th class="py-3 px-6 text-center">Amount</th>
                            </tr>
                        </thead>
                        <tbody class="text-gray-600 text-sm font-light">
                            <h1 class="text-3xl mb-5">Orders</h1>
                            @foreach ($orders as $order)
                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        {{$order->name}}
                                    </td>
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        {{$order->email}}
                                    </td>
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        {{$order->origin}}
                                    </td>
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        {{$order->mode}}
                                    </td>
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        {{$order->weight}}
                                    </td>
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        NGN {{$order->amount}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
