
    <form action=" {{route('pay')}} " method="POST">
        {{ csrf_field() }}
        <div class="md:grid grid-cols-1 md:grid-cols-2 md:gap-x-10 gap-y-5 space-y-5 md:space-y-0">
            <div class=" ">
                <label class="block font-medium text-base text-gray-700" for="">Your Name</label>
                <input type="text" name="name" wire:model="name" required class=" w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
            </div>
            <div class=" ">
                <label class="block font-medium text-base text-gray-700" for="">Your Email</label>
                <input type="email" name="email" wire:model="email" required class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
            </div>
            <div>
                <label class="block font-medium text-base text-gray-700" for="">Country of Origin</label>
                {{-- <input type="text" class=" w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"> --}}
                <select required wire:model="origin" name="origin" id="" class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                    <option selected value="USA">United States</option>
                    <option value="UK">United Kingdom</option>
                </select>
            </div>
            <div>
                <label class="block font-medium text-base text-gray-700" for="">Mode of Transport</label>
                {{-- <input type="text" > --}}
                <select required wire:model="mode" name="mode" class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
                    <option selected value="Air">Air</option>
                    <option value="Sea">Sea</option>
                </select>
                @if ( $mode == "Air" )
                    <p class=" text-sm text-gray-500 mt-2">Transportation by Sea takes 10 times longer than by Air</p>
                @endif
                @if ( $mode == "Sea" )
                    <p class=" text-sm text-gray-500 mt-2">Arrives in Nigeria two days after shipment </p>
                @endif
            </div>
            <div>
                <label class="block font-medium text-base text-gray-700" for="">Weight of Item (kg)</label>
                <input required type="number" name="weight" wire:model="weight" class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50">
            </div>
            <div class=" col-start-1 col-span-2">
                <p class="text-lg text-center mt-12 mb-4">Total Amount</p>
                <h1 class=" text-6xl text-center text-green-500 font-medium">NGN {{$total}} </h1>
                <p class="text-lg text-gray-500 text-center mt-4 mb-4">Delivery Period 2 days</p>
            </div>
            <div class=" col-start-1 col-span-2 flex justify-center">
                <input wire:model="total" type="hidden" name="amount" /> <!-- Replace the value with your transaction amount -->
                <input type="hidden" name="payment_method" value="both" /> <!-- Can be card, account, both -->
                <input type="hidden" name="description" value=" {{$name}} Consignment" /> <!-- Replace the value with your transaction description -->
                <input type="hidden" name="country" value="NG" /> <!-- Replace the value with your transaction country -->
                <input type="hidden" name="currency" value="NGN" /> <!-- Replace the value with your transaction currency -->
                {{-- <input type="hidden" name="paymentplan" value="362" /> <!-- Ucomment and Replace the value with the payment plan id --> --}}
                {{-- <input type="hidden" name="ref" value="MY_NAME_5uwh2a2a7f270ac98" /> <!-- Ucomment and  Replace the value with your transaction reference. It must be unique per transaction. You can delete this line if you want one to be generated for you. --> --}}
                {{-- <input type="hidden" name="logo" value="https://pbs.twimg.com/profile_images/915859962554929153/jnVxGxVj.jpg" /> <!-- Replace the value with your logo url (Optional, present in .env)--> --}}
                {{-- <input type="hidden" name="title" value="Flamez Co" /> <!-- Replace the value with your transaction title (Optional, present in .env) --> --}}
                <button type="submit" class=" w-60 bg-green-500 py-3 shadow text-white">Make Payment</button>
            </div>
        </div>
    </form>


